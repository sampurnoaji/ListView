package com.example.xsisacademy.listview.Adapter;

/**
 * Created by XsisAcademy on 21/06/2019.
 */

public class DataModel {
    private String nama, kota;

    public DataModel(String nama, String kota) {
        this.nama = nama;
        this.kota = kota;
    }

    public String getNama() {
        return nama;
    }

    public String getKota() {
        return kota;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }
}
