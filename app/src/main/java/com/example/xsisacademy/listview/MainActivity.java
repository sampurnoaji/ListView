package com.example.xsisacademy.listview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnLVStatis, btnLVDinamis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolListener();
    }

    private void toolListener() {
        btnLVStatis = findViewById(R.id.btnLVStatis);
        btnLVDinamis = findViewById(R.id.btnLVDinamis);

        btnLVStatis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getBaseContext(), LVStatisActivity.class));
            }
        });
        btnLVDinamis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getBaseContext(), LVDinamisActivity.class));
            }
        });

    }
}
