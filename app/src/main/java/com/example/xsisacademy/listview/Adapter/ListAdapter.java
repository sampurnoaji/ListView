package com.example.xsisacademy.listview.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.xsisacademy.listview.R;

import java.util.ArrayList;

/**
 * Created by XsisAcademy on 21/06/2019.
 */

public class ListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<DataModel> dataModelArrayList;

    public ListAdapter(Context context, ArrayList<DataModel> dataModelArrayList) {
        this.context = context;
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public int getCount() {
        return dataModelArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataModelArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_list, null, true);

            holder.nama = view.findViewById(R.id.itemNama);
            holder.kota = view.findViewById(R.id.itemKota);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.nama.setText("Nama: " + dataModelArrayList.get(i).getNama());
        holder.kota.setText("Kota: " + dataModelArrayList.get(i).getKota());

        return view;
    }

    private class ViewHolder{
        TextView nama, kota;
    }
}
