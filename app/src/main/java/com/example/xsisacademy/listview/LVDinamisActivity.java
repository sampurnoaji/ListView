package com.example.xsisacademy.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.ListView;

import com.example.xsisacademy.listview.Adapter.DataModel;
import com.example.xsisacademy.listview.Adapter.ListAdapter;

import java.util.ArrayList;

public class LVDinamisActivity extends AppCompatActivity {

    private ListView listView;
    private ArrayList<DataModel> dataModels = new ArrayList<>();
    private ListAdapter adapter;
    private DataModel data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lvdinamis);

        populateDummy();
        setupView();
    }

    private void setupView() {
        listView = findViewById(R.id.listView2);

        adapter = new ListAdapter(this, dataModels);
        listView.setAdapter(adapter);
    }

    private void populateDummy() {
        data = new DataModel("Rudolf", "Papua");
        dataModels.add(data);

        data = new DataModel("Kandar", "Banyumas");
        dataModels.add(data);
    }
}
